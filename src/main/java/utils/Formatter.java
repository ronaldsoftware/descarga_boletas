package utils;

import constants.Constantes;

public class Formatter {

	public static String monthToText(String month) {
		String monthText = "Mes invalido";

		switch (month) {
		case "01":
			monthText = "Enero";
			break;
		case "02":
			monthText = "Febrero";
			break;
		case "03":
			monthText = "Marzo";
			break;
		case "04":
			monthText = "Abril";
			break;
		case "05":
			monthText = "Mayo";
			break;
		case "06":
			monthText = "Junio";
			break;
		case "07":
			monthText = "Julio";
			break;
		case "08":
			monthText = "Agosto";
			break;
		case "09":
			monthText = "Setiembre";
			break;
		case "10":
			monthText = "Octubre";
			break;
		case "11":
			monthText = "Noviembre";
			break;
		case "12":
			monthText = "Diciembre";
			break;
		}

		return monthText;
	}

	public static String nombreBoletaAppMovil(String codigoEmpleado, String codigoProceso, String periodo) {
		return codigoEmpleado + Constantes.GUION_BAJO + periodo + Constantes.EXTENSION_PDF;
	}

}
