package utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import main.InicioDesktopApp;

public class DBConnection {
	private static Logger log = LoggerFactory.getLogger(InicioDesktopApp.class);

	public static Connection conexion;

	public static void setConexion(Properties propiedades) throws SQLException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(propiedades.getProperty("driverclassname"));
		dataSource.setUrl(propiedades.getProperty("url"));
		dataSource.setUsername(propiedades.getProperty("username"));
		dataSource.setPassword(propiedades.getProperty("password"));
		conexion = dataSource.getConnection();
	}

	public static void cerrarConexion() {
		try {
			if (conexion != null) {
				conexion.close();
			}
		} catch (SQLException e) {
			log.error("Error conexi�n BD", e);
		}
	}

}
