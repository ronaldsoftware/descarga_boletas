package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.jfree.util.Log;

public class FileReader {

	static public Properties readProperties(String filename) {

		Properties props = new Properties();
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream stream = loader.getResourceAsStream(filename);
			props.load(stream);
		} catch (Exception e) {
			Log.error("Error leer properties", e);
		}

		return props;
	}

	static public String readTextFile(String filename) {
		StringBuilder contentBuilder = new StringBuilder();

		try {
			FileInputStream input = new FileInputStream(
					FileReader.class.getResource(filename).getPath().replace("%20", " "));
			InputStreamReader reader = new InputStreamReader(input, "UTF-8");
			BufferedReader in = new BufferedReader(reader);
			String str;
			while ((str = in.readLine()) != null) {
				contentBuilder.append(str);
			}
			in.close();
			input.close();
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return contentBuilder.toString();
	}

	public static byte[] readBytesFromFile(String filePath) {
		FileInputStream fileInputStream = null;
		byte[] bytesArray = null;

		try {

			File file = new File(filePath);
			bytesArray = new byte[(int) file.length()];

			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bytesArray);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bytesArray;

	}
}
