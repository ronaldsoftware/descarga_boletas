package dao.impl;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dao.BoletaDAO;
import oracle.jdbc.internal.OracleTypes;
import utils.DBConnection;

public class UniconBoletaDAO implements BoletaDAO {
	private static Logger log = LoggerFactory.getLogger(UniconBoletaDAO.class);

	public ArrayList<SubreporteDiasLaborados> listarDiasTrabajados(String codcia, String codsuc, String nroper,
			String codpro, String codtra) {
		ArrayList<SubreporteDiasLaborados> lista = new ArrayList<SubreporteDiasLaborados>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_BOLETA.GET_HABERES_DIAS(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, codcia);
			cstmt.setString(3, codsuc);
			cstmt.setString(4, nroper);
			cstmt.setString(5, codpro);
			cstmt.setString(6, codtra);

			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				SubreporteDiasLaborados concepto = new SubreporteDiasLaborados();
				concepto.setNombreConcepto(rs.getString("CONDES"));
				concepto.setValor(rs.getDouble("VALOR"));
				concepto.setDias(rs.getDouble("DIAS"));
				concepto.setHoras(rs.getDouble("HORAS"));
				lista.add(concepto);
			}

		} catch (Exception e) {
			log.error("Error en jdbc", e);
		} finally {
			try {
				rs.close();
				cstmt.close();
			} catch (SQLException e) {
			}
		}

		return lista;
	}

	public ArrayList<VacacionesCabBean> listarVacaciones(String codcia, String codsuc, String periodoIni,
			String periodoFin, String codtra, String codpro) {
		VacacionesCabBean vaca = null;
		ArrayList<VacacionesCabBean> lista = new ArrayList<VacacionesCabBean>();
		CallableStatement cstmt = null;
		ResultSet rs = null;

		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_RH_EMPLEADO.GET_VAC_EMP_PER(?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, codcia);
			cstmt.setString(3, codsuc);
			cstmt.setString(4, periodoIni);
			cstmt.setString(5, periodoFin);
			cstmt.setString(6, codtra);
			cstmt.setString(7, codpro);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				vaca = new VacacionesCabBean();
				vaca.setVACCODCIA(rs.getString("VACCODCIA"));
				vaca.setVACCODSUC(rs.getString("VACCODSUC"));
				vaca.setVACCODTRA(rs.getString("VACCODTRA"));
				vaca.setVACNROCTL(rs.getString("VACNROCTL"));
				vaca.setVACTIPVAC(rs.getString("VACTIPVAC"));
				vaca.setVACFECINI(rs.getString("FECINI"));
				vaca.setVACDIASPRO(rs.getString("VACDIASPRO"));
				vaca.setVACFECFIN(rs.getString("FECFIN"));
				vaca.setVACCODPRO(rs.getString("VACCODPRO"));
				vaca.setVACPERPRO(rs.getString("VACPERPRO"));
				vaca.setVACFLGEST(rs.getString("VACFLGEST"));
				vaca.setVACFLGPRO(rs.getString("VACFLGPRO"));
				vaca.setVACPERVA1(rs.getString("VACPERVA1"));
				vaca.setVACPERVA2(rs.getString("VACPERVA2"));
				vaca.setVACGLOSA(rs.getString("VACGLOSA"));
				vaca.setVACTIPVAR(rs.getString("VACTIPVAR"));
				vaca.setVACCORREL(rs.getString("VACCORREL"));
				lista.add(vaca);
			}
		} catch (Exception e) {
			log.error("Error en jdbc", e);
		} finally {
			try {
				rs.close();
				cstmt.close();
			} catch (SQLException e) {
			}
		}

		return lista;

	}

	public ArrayList<ReporteSubreporte> listarMontosAdicionales(String pCodcia, String pCodsuc, String pCodpro,
			String pPeriodo, String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal,
			String pCcosto) {
		CallableStatement cstmt = null;
		ArrayList<ReporteSubreporte> lista = new ArrayList<ReporteSubreporte>();
		ReporteSubreporte s = null;
		ResultSet rs = null;

		try {
			cstmt = DBConnection.conexion
					.prepareCall("{?= call PKG_BOLETA.GET_MONTO_ADICIONALES(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodpro);
			cstmt.setString(5, pPeriodo);
			cstmt.setString(6, pPeriodoFin);
			cstmt.setString(7, pCodtra);
			cstmt.setString(8, pPuesto);
			cstmt.setString(9, pArea);
			cstmt.setString(10, pLocal);
			cstmt.setString(11, pCcosto);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				s = new ReporteSubreporte();
				s.setCodtra(rs.getString(1));
				s.setCodcon(rs.getString("nomcodcon"));
				s.setDescripcion(rs.getString("con_apor"));
				s.setMonto(rs.getString("val_apor"));
				s.setNomvalpar(rs.getString("nomvalpar"));
				lista.add(s);
			}
		} catch (Exception e) {
			log.error("Error en jdbc", e);
		} finally {
			try {
				rs.close();
				cstmt.close();
			} catch (SQLException e) {
			}
		}

		return lista;
	}

	@Override
	public CompaniaBean obtenerCompania(String codcia) {
		CompaniaBean compania = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = DBConnection.conexion.prepareStatement("SELECT * FROM EBCOMPANIA WHERE EBCODCIA = ?");
			pst.setString(1, codcia);
			pst.execute();
			rs = pst.executeQuery();
			if (rs.next()) {
				compania = new CompaniaBean();
				compania.setEBCODCIA(rs.getString("EBCODCIA"));
				compania.setEBCODSUC(rs.getString("EBCODSUC"));
				compania.setEBDESCIA(rs.getString("EBDESCIA"));
				compania.setEBDESSUC(rs.getString("EBDESSUC"));
				compania.setEBDIRECCION(rs.getString("EBDIRECCION"));
				compania.setEBREPRESENTANTE(rs.getString("EBREPRESENTANTE"));
				compania.setEBCARGOREP(rs.getString("EBCARGOREP"));
				compania.setEBDNIREPRE(rs.getString("EBDNIREPRE"));
				compania.setEBEMAIL(rs.getString("EBEMAIL"));
				compania.setEBNRORUC(rs.getString("EBNRORUC"));
				compania.setEBTELF1(rs.getString("EBTELF1"));
				compania.setEBTELF2(rs.getString("EBTELF2"));
				compania.setEBTELF3(rs.getString("EBTELF3"));
				compania.setEBWEBSITE(rs.getString("EBWEBSITE"));
				compania.setEBLOGO(rs.getString("EBLOGO"));
				compania.setEBREPRESENTANTEFIRMA(rs.getString("EBREPRESENTANTEFIRMA"));
				compania.setEBREPRESENTANTEFIRMA2(rs.getString("EBREPRESENTANTEFIRMA2"));
				compania.setEBLONGITUD(rs.getString("EBLONGITUD"));
				compania.setEBLATITUD(rs.getString("EBLATITUD"));
			}
		} catch (Exception e) {
			log.error("Error en jdbc", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
			}
		}

		return compania;
	}

	@Override
	public List<BeanHR_EMPLEADO> listarEmpleadoXProceso(String codcia, String codsuc, String flag, String periodo,
			String proceso) {
		List<BeanHR_EMPLEADO> lista = new ArrayList<BeanHR_EMPLEADO>();
		ResultSet rs = null;
		CallableStatement cstmt = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_AL_ALERTA.GET_EMPXPROC2(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, codcia);
			cstmt.setString(3, codsuc);
			cstmt.setString(4, flag);
			cstmt.setString(5, periodo);
			cstmt.setString(6, proceso);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				BeanHR_EMPLEADO item = new BeanHR_EMPLEADO();
				item.setEMPCODCIA(rs.getString("EMPCODCIA"));
				item.setEMPCODTRA(rs.getString("EMPCODTRA"));
				item.setEMPAPATERN(rs.getString("EMPAPATERN"));
				item.setEMPAMATERN(rs.getString("EMPAMATERN"));
				item.setEMPNOMBRE(rs.getString("EMPNOMBRE"));
				item.setEMPEMAIL(rs.getString("EMPEMAIL"));
				item.setEMPCODPRO(rs.getString("EMPCODPRO"));
				item.setEMPNRODOCID(rs.getString("EMPNRODOCID"));
				lista.add(item);
			}
		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		} finally {
			try {
				rs.close();
				cstmt.close();
			} catch (SQLException e) {
			}
		}
		return lista;
	}

	@Override
	public List<ReporteBoletaPago> listarPrincipalBoleta(String pCodcia, String pCodsuc, String pCodpro,
			String pPeriodo, String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal,
			String pCcosto) {
		CallableStatement cstmt = null;
		ArrayList<ReporteBoletaPago> lista = new ArrayList<ReporteBoletaPago>();
		ReporteBoletaPago b = null;
		try {
			cstmt = DBConnection.conexion
					.prepareCall("{?= call PKG_BOLETA.GET_EMPLEADO_BOL_GENERAL(?,?,?,?,?,?,?,?,?,?)}");

			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodpro);
			cstmt.setString(5, pPeriodo);
			cstmt.setString(6, pPeriodoFin);
			cstmt.setString(7, pCodtra);
			cstmt.setString(8, pPuesto);
			cstmt.setString(9, pArea);
			cstmt.setString(10, pLocal);
			cstmt.setString(11, pCcosto);
			cstmt.execute();
			ResultSet rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				b = new ReporteBoletaPago();

				b.setRazon(rs.getString("DESCIA"));
				b.setRemype("0000073022-2009");
				b.setDom(rs.getString("DIRECCION"));
				b.setRuc(rs.getString("RUC"));
				b.setNombre(rs.getString("EMPNOM"));
				b.setNombreE(rs.getString("nombreE"));
				b.setApellidoP(rs.getString("apellidoP"));
				b.setApellidoM(rs.getString("apellidoM"));
				b.setCargo(rs.getString("PUESDES"));
				b.setFecing(rs.getString("FECING"));
//				b.setDialab(rs.getString("DIAS_LAB"));
				b.setEssalud(rs.getString("EMPESSALUD"));
//				b.setSobretiempo(rs.getString("HREX"));
//				b.setNormales(rs.getString("hnormales"));
//				b.setHnormales(rs.getString("hnormales"));
//				b.setH25(rs.getString("HREX25"));
//				b.setH35(rs.getString("HREX35"));
//				b.setH100(rs.getString("HREX100"));

//				if (pCodcia.equals("029") || pCodcia.equals("034")) {
//					b.setSobretiempo(rs.getString("SOBRETIEMPO"));
//					b.setDias_pag(rs.getString("DIAS_PAG"));
//					b.setHrex35dia(rs.getString("HREX35DIA"));
//					b.setHrex35noc(rs.getString("HREX35NOC"));
//					b.setSemana(rs.getString("SEMANA"));
//				}

				b.setSueldo(rs.getString("REMU"));
				b.setCategoria(rs.getString("tipoemp"));
				b.setDni(rs.getString("EMPNRODOCID"));
				b.setSpp(rs.getString("NROESS"));
//				b.setTardanzas(rs.getString("MTARD"));
				b.setAfp(rs.getString("AFPDES"));
				b.setCusp(rs.getString("EMPNROAFP"));
				b.setCalificacion(rs.getString("situesp"));
				b.setPervac(rs.getString("periodo_vac"));
				b.setSalida(rs.getString("salida_vac"));
				b.setRetorno(rs.getString("retorno_vac"));
				b.setCodigo(rs.getString("EMPCODTRA"));
				b.setTremu(rs.getString("HAB"));
				b.setTdsc(rs.getString("DES"));
				b.setTapor(rs.getString("APO"));
				b.setNeto(rs.getString("NET"));

				b.setRfecha(rs.getString("RANGO_FECHA"));
				b.setBanco(rs.getString("BANCTHAB"));
				b.setNrocuenta(rs.getString("EMPNROCTHAB"));
				b.setAsig(rs.getString("asig"));
				b.setFecret(rs.getString("fecret"));
				b.setFecha(rs.getString("fecnac"));
				b.setPeriodo(rs.getString("periodo"));
				b.setCodant(rs.getString("empcodant"));
				b.setSituesp(rs.getString("SITUESP"));
				b.setAnio_periodo(rs.getString("anio_periodo"));
				b.setMes_periodo(rs.getString("mes_periodo"));
				b.setArea(rs.getString("aredes"));
				b.setCcosto(rs.getString("ccosto_des"));
				b.setLocal(rs.getString("locdes"));
				b.setTotalprestamo(rs.getString("totalprestamo"));
				b.setPagadoprestamo(rs.getString("pagadoprestamo"));
				b.setSaldoprestamo(rs.getString("saldoprestamo"));
//				b.setDMEDL(rs.getString("DMEDL"));
				// c.aguirre # campo telefono compania
				b.setTelefono(rs.getString("TELEFONO"));
				// c.aguirre # 10 parametros para aportes
				b.setCodtra(rs.getString("EMPCODTRA"));
				b.setCodcia(rs.getString("empcodcia"));
				b.setCodsuc(rs.getString("empcodsuc"));
				b.setPini(rs.getString("per"));
				b.setPfin(rs.getString("perfin"));
				b.setPcodpuest(rs.getString("pcodpuest"));
				b.setPcodarea(rs.getString("pcodarea"));
				b.setPcodloc(rs.getString("pcodloc"));
				b.setPccosto(rs.getString("pccosto"));
				b.setPro(rs.getString("pro"));
				b.setSexo(rs.getString("empsexo"));
				b.setFecnac(rs.getString("fecnac"));
				b.setPaisnac(rs.getString("NACIONALIDAD"));
				b.setDireccion(rs.getString("EMPDIRECCION"));
				b.setCatocup(rs.getString("CATEG_OCUP"));
				b.setMontoAportAdic1(rs.getString("MONTOS_ADICIONALES"));

				b.setDepartamento(rs.getString("departamento"));
				b.setProvincia(rs.getString("provincia"));
				b.setDistrito(rs.getString("distrito"));
				b.setEmpcodafp(rs.getString("empcodafp"));
				b.setMesNumero(rs.getString("mes_numero"));
//				b.setNetoletra(rs.getString("Netoletra"));
//				b.setNetoletra("");

				lista.add(b);
			}
			rs.close();
			cstmt.close();

		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		}

		return lista;
	}

	@Override
	public List<ReporteSubreporte> listarHaberes(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto) {
		CallableStatement cstmt = null;
		ArrayList<ReporteSubreporte> lista = new ArrayList<ReporteSubreporte>();
		ReporteSubreporte s = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_BOLETA.GET_HABERES_BOL2(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodpro);
			cstmt.setString(5, pPeriodo);
			cstmt.setString(6, pPeriodoFin);
			cstmt.setString(7, pCodtra);
			cstmt.setString(8, pPuesto);
			cstmt.setString(9, pArea);
			cstmt.setString(10, pLocal);
			cstmt.setString(11, pCcosto);

			cstmt.execute();
			ResultSet rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				s = new ReporteSubreporte();
				s.setCodtra(rs.getString(1));
				s.setCodcon(rs.getString("nomnroper"));
				s.setDescripcion(rs.getString("con_ing"));
				s.setMonto(rs.getString("val_ing"));
				s.setNomvalpar(rs.getString("nomvalpar"));
				lista.add(s);
			}
			rs.close();
			cstmt.close();

		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		}

		return lista;
	}

	@Override
	public List<ReporteSubreporte> listarDescuentos(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto) {
		CallableStatement cstmt = null;
		ArrayList<ReporteSubreporte> lista = new ArrayList<ReporteSubreporte>();
		ReporteSubreporte s = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_BOLETA.GET_DESCUENTOS_BOL2(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodpro);
			cstmt.setString(5, pPeriodo);
			cstmt.setString(6, pPeriodoFin);
			cstmt.setString(7, pCodtra);
			cstmt.setString(8, pPuesto);
			cstmt.setString(9, pArea);
			cstmt.setString(10, pLocal);
			cstmt.setString(11, pCcosto);
			cstmt.execute();
			ResultSet rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				s = new ReporteSubreporte();
				s.setCodtra(rs.getString(1));
				s.setCodcon(rs.getString("nomnroper"));
				s.setDescripcion(rs.getString("con_desc"));
				s.setMonto(rs.getString("val_desc"));
				s.setNomvalpar(rs.getString("nomvalpar"));
				lista.add(s);
			}
			rs.close();
			cstmt.close();
		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		}

		return lista;
	}

	@Override
	public List<ReporteSubreporte> listarAportes(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto) {
		CallableStatement cstmt = null;
		ArrayList<ReporteSubreporte> lista = new ArrayList<ReporteSubreporte>();
		ReporteSubreporte s = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_BOLETA.GET_APORTES_BOL2(?,?,?,?,?,?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodpro);
			cstmt.setString(5, pPeriodo);
			cstmt.setString(6, pPeriodoFin);
			cstmt.setString(7, pCodtra);
			cstmt.setString(8, pPuesto);
			cstmt.setString(9, pArea);
			cstmt.setString(10, pLocal);
			cstmt.setString(11, pCcosto);
			cstmt.execute();
			ResultSet rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				s = new ReporteSubreporte();
				s.setCodtra(rs.getString(1));
				s.setCodcon(rs.getString("nomnroper"));
				s.setDescripcion(rs.getString("con_apor"));
				s.setMonto(rs.getString("val_apor"));
				s.setNomvalpar(rs.getString("nomvalpar"));
				lista.add(s);
			}
			rs.close();
			cstmt.close();
		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		}

		return lista;
	}

	@Override
	public List<SubreporteAsistencias> listarAsistencias(String pCodcia, String pCodsuc, String pCodtra, String pAnio, Integer pMes) {


		CallableStatement cstmt = null;
		ArrayList<SubreporteAsistencias> lista = new ArrayList<SubreporteAsistencias>();
		SubreporteAsistencias s = null;
		try {
			cstmt = DBConnection.conexion.prepareCall("{?= call PKG_BOLETA.GET_ASISTENCIAS(?,?,?,?,?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);
			cstmt.setString(2, pCodcia);
			cstmt.setString(3, pCodsuc);
			cstmt.setString(4, pCodtra);
			cstmt.setString(5, pAnio);
			cstmt.setInt(6, pMes);
			cstmt.execute();
			ResultSet rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				s = new SubreporteAsistencias();
				s.setEmpresa(rs.getString("empresa"));
				s.setRuc(rs.getString("ruc"));
				s.setTrabajador(rs.getString("trabajador"));
				s.setDni(rs.getString("dni"));
				s.setCodigo(rs.getString("codigo"));
				s.setPfecha(rs.getString("pfecha"));
				s.setInicio(rs.getString("inicio"));
				s.setFin(rs.getString("fin"));
				s.setEntrada(rs.getString("entrada"));
				s.setSalida(rs.getString("salida"));
				s.setPdsccomp(rs.getDouble("PDSCCOMP"));
				s.setPhorasferiado(rs.getDouble("PHORASFERIADO"));
				s.setPfaltasinjustif(rs.getDouble("PFALTASINJUSTIF"));
				s.setPfaltasjustifi(rs.getDouble("PFALTASJUSTIFI"));
				s.setPdmedsubslicpat(rs.getDouble("PDMEDSUBSLICPAT"));
				s.setPvacaciones(rs.getDouble("PVACACIONES"));
				s.setPhorastotales(rs.getDouble("PHORASTOTALES"));
				s.setPhorascontrol(rs.getDouble("PHORASCONTROL"));
				s.setPdescprovin(rs.getDouble("PDESCPROVIN"));
				s.setPhextra100(rs.getDouble("PHEXTRA100"));
				s.setPeriodo(rs.getString("periodo"));
				s.setPtotdomini(rs.getDouble("Ptotdomini"));
				s.setTotphorasferiado(rs.getDouble("Phorasferiado"));
				s.setTotphorascontrol(rs.getDouble("totPhorascontrol"));
				s.setPhe50(rs.getDouble("Phe50"));
				s.setPh100(rs.getDouble("Ph100"));
				lista.add(s);
			}
			rs.close();
			cstmt.close();
		} catch (SQLException e) {
			log.error("Error en jdbc", e);
		}

		return lista;
	}

}