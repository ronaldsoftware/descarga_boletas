package dao;

import java.util.List;

import model.*;

public interface BoletaDAO {

	public List<BeanHR_EMPLEADO> listarEmpleadoXProceso(String codcia, String codsuc, String flag, String periodo,
			String proceso);

	public CompaniaBean obtenerCompania(String codcia);

	public List<ReporteBoletaPago> listarPrincipalBoleta(String pCodcia, String pCodsuc, String pCodpro,
			String pPeriodo, String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal,
			String pCcosto);

	public List<ReporteSubreporte> listarHaberes(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto);

	public List<ReporteSubreporte> listarDescuentos(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto);

	public List<ReporteSubreporte> listarAportes(String pCodcia, String pCodsuc, String pCodpro, String pPeriodo,
			String pPeriodoFin, String pCodtra, String pPuesto, String pArea, String pLocal, String pCcosto);

	public List<SubreporteAsistencias> listarAsistencias(String pCodcia, String pCodsuc, String pCodtra, String pAnio, Integer pMes);



}
