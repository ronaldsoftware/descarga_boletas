package main;

import javax.swing.JFrame;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import component.PanelPantallaInicial;

public class InicioDesktopApp {
	private static Logger log = LoggerFactory.getLogger(InicioDesktopApp.class);
	private final static String TITULO_APP = "BOLETAS";
	private final static int ANCHO_VENTANA = 600;
	private final static int ALTO_VENTANA = 500;

	public static void main(String[] args) {
		PanelPantallaInicial pantallaInicial = new PanelPantallaInicial();
		JFrame frame = new JFrame();
		frame.setTitle(TITULO_APP);
		frame.setSize(ANCHO_VENTANA, ALTO_VENTANA);
		frame.setResizable(false);
		frame.add(pantallaInicial.getPantalla());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		log.debug("Se cre� la ventana de la aplicaci�n {} con dimensiones {} x {}", TITULO_APP, ANCHO_VENTANA,
				ALTO_VENTANA);
	}
}
