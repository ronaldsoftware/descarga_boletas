package service;

import java.util.Properties;

public interface EmailService {

	public boolean enviarBoleta(String to, String subject, String body, String... rutasFile);

	public void setEmailProperties(Properties properties);

}
