package service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfWriter;

import constants.Constantes;
import constants.ConstantesCompania;
import dao.BoletaDAO;
import dao.impl.ComsatelBoletaDAO;
import model.BeanHR_EMPLEADO;
import model.CompaniaBean;
import model.Configuracion;
import model.ReporteBoletaPago;
import model.ReporteSubreporte;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import service.BoletaService;
import utils.DBConnection;
import utils.FileReader;

public class ComsatelBoletaService implements BoletaService {
	private static Logger log = LoggerFactory.getLogger(ComsatelBoletaService.class);

	private BoletaDAO boletaDAO = new ComsatelBoletaDAO();

	@Override
	public int descargaMasivaBoletas(Configuracion configuracion) {
		Properties companiaProperties = FileReader.readProperties(ConstantesCompania.PROPERTIES_COMSATEL);
		int cantidaTotal = 0;

		String[] procesos = configuracion.getProceso().split(Constantes.COMA);

		try {
			DBConnection.setConexion(companiaProperties);

			CompaniaBean compania = boletaDAO.obtenerCompania(configuracion.getCompania());

			for (String proceso : procesos) {
				int cantidadEmpleado = 0;
				int cantidadImprimir = 10;

				proceso = proceso.trim();
				List<BeanHR_EMPLEADO> empleados = boletaDAO.listarEmpleadoXProceso(compania.getEBCODCIA(),
						compania.getEBCODSUC(), Constantes.ESTADO_PENDIENTE, configuracion.getPeriodo(), proceso);

				for (BeanHR_EMPLEADO empleado : empleados) {
					List<ReporteBoletaPago> boletaPrincipal = boletaDAO.listarPrincipalBoleta(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> haberes = boletaDAO.listarHaberes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> descuentos = boletaDAO.listarDescuentos(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> aportes = boletaDAO.listarAportes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					for (ReporteBoletaPago reporteBoleta : boletaPrincipal) {
						reporteBoleta.setNombre(reporteBoleta.getNombreCompleto());

						reporteBoleta.setListaRemuneracion((ArrayList<ReporteSubreporte>) haberes);
						reporteBoleta.setListaDeduccion((ArrayList<ReporteSubreporte>) descuentos);
						reporteBoleta.setListaAportacion((ArrayList<ReporteSubreporte>) aportes);
					}

					Map<String, Object> parametros = new HashMap<>();
					parametros.put("SUBREPORT_DIR",
							getClass().getResource(companiaProperties.getProperty("ruta_reporte")).getPath());
					parametros.put("ruta", companiaProperties.getProperty("ruta"));
					parametros.put("firma", getClass().getResource(companiaProperties.getProperty("firma")).getPath());
					parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo")).getPath());
					parametros.put("p_cnn", DBConnection.conexion);

					try {

						String rutaReporte = companiaProperties.getProperty("ruta_reporte")
								+ companiaProperties.getProperty("reporte");

						JasperReport reporte = (JasperReport) JRLoader
								.loadObject(getClass().getResource((rutaReporte)));

						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros,
								new JRBeanCollectionDataSource(boletaPrincipal));

						JRPdfExporter exporter = new JRPdfExporter();
						exporter.setExporterInput(new SimpleExporterInput(jasperPrint));

						exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(configuracion.getRutaDescarga()
								+ Constantes.SLASH
								+ nombreBoletaAppMovil(empleado.getEMPCODTRA(), proceso, configuracion.getPeriodo())));

						if (configuracion.isCifrado()) {
							SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
							configuration.setEncrypted(true);
							configuration.set128BitKey(true);
							configuration.setUserPassword(empleado.getEMPNRODOCID());
							configuration.setOwnerPassword(empleado.getEMPNRODOCID());
							configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
							exporter.setConfiguration(configuration);
						}

						exporter.exportReport();

						cantidadEmpleado++;

						if (cantidadEmpleado >= cantidadImprimir) {
							log.info("Se est�n descargando " + cantidadImprimir + " boletas...");
							cantidadImprimir += 10;
						}

					} catch (JRException e) {
						log.error("Error generando reporte", e);
					}
				}
				log.info("Se han descargado " + cantidadEmpleado + " boletas del proceso " + proceso);
				cantidaTotal += cantidadEmpleado;
			}

		} catch (SQLException e) {
			log.error("Error en conexion BD", e);
		} finally {
			DBConnection.cerrarConexion();
		}

		return cantidaTotal;
	}

	private String nombreBoletaAppMovil(String codigoEmpleado, String codigoProceso, String periodo) {
		return codigoEmpleado + Constantes.GUION_BAJO + codigoProceso + Constantes.GUION_BAJO + periodo
				+ Constantes.EXTENSION_PDF;
	}

	@Override
	public int envioMasivoBoletas(Configuracion configuracion) {
		// TODO Auto-generated method stub
		return 0;
	};
}
