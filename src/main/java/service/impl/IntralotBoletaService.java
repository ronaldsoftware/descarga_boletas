package service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfWriter;

import constants.Constantes;
import constants.ConstantesCompania;
import dao.BoletaDAO;
import dao.impl.IntralotBoletaDAO;
import model.BeanHR_EMPLEADO;
import model.CompaniaBean;
import model.Configuracion;
import model.ReporteBoletaPago;
import model.ReporteSubreporte;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import service.BoletaService;
import service.EmailService;
import utils.DBConnection;
import utils.FileReader;

public class IntralotBoletaService implements BoletaService {
	private static Logger log = LoggerFactory.getLogger(IntralotBoletaService.class);

	private static final String CONCEPTO_ESSALUD = "3010";
	private static final String CONCEPTO_SENATI = "3030";

	private BoletaDAO boletaDAO = new IntralotBoletaDAO();
	private EmailService emailService = new EmailServiceImpl();

	@Override
	public int descargaMasivaBoletas(Configuracion configuracion) {
		Properties companiaProperties = FileReader.readProperties(ConstantesCompania.PROPERTIES_INTRALOT);
		int cantidaTotal = 0;

		String[] procesos = configuracion.getProceso().split(Constantes.COMA);

		try {
			DBConnection.setConexion(companiaProperties);

			CompaniaBean compania = boletaDAO.obtenerCompania(configuracion.getCompania());

			for (String proceso : procesos) {
				int cantidadEmpleados = 0;
				int cantidadImprimir = 10;

				proceso = proceso.trim();
				List<BeanHR_EMPLEADO> empleados = boletaDAO.listarEmpleadoXProceso(compania.getEBCODCIA(),
						compania.getEBCODSUC(), Constantes.ESTADO_PROCESADO, configuracion.getPeriodo(), proceso);

				for (BeanHR_EMPLEADO empleado : empleados) {
					List<ReporteBoletaPago> boletaPrincipal = boletaDAO.listarPrincipalBoleta(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> haberes = boletaDAO.listarHaberes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> descuentos = boletaDAO.listarDescuentos(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> aportes = boletaDAO.listarAportes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					for (ReporteBoletaPago reporteBoleta : boletaPrincipal) {
						reporteBoleta.setNombre(reporteBoleta.getNombreCompleto());

						reporteBoleta.setListaRemuneracion((ArrayList<ReporteSubreporte>) haberes);
						reporteBoleta.setListaDeduccion((ArrayList<ReporteSubreporte>) descuentos);
						reporteBoleta.setListaAportacion((ArrayList<ReporteSubreporte>) aportes);

						for (ReporteSubreporte aporte : aportes) {
							if (CONCEPTO_ESSALUD.equals(aporte.getCodcon().trim())) {
								reporteBoleta.setMontoEssalud(aporte.getMonto());
							}

							if (CONCEPTO_SENATI.equals(aporte.getCodcon().trim())) {
								reporteBoleta.setMontoSenati(aporte.getMonto());
							}
						}

					}
					Map<String, Object> parametros = new HashMap<>();
					parametros.put("SUBREPORT_DIR",
							getClass().getResource(companiaProperties.getProperty("ruta_reporte")).getPath());
					parametros.put("ruta", getClass().getResource(companiaProperties.getProperty("firma")).getPath());
					parametros.put("firma", compania.getEBREPRESENTANTE() == null ? "" : compania.getEBREPRESENTANTE());
					parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo")).getPath());
					parametros.put("p_cnn", DBConnection.conexion);
					parametros.put("firmaRRHH", true);
					parametros.put("cargorep", compania.getEBCARGOREP() == null ? "" : compania.getEBCARGOREP());

					try {

						String rutaReporte = companiaProperties.getProperty("ruta_reporte")
								+ companiaProperties.getProperty("reporte");

						JasperReport reporte = (JasperReport) JRLoader
								.loadObject(getClass().getResource((rutaReporte)));

						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros,
								new JRBeanCollectionDataSource(boletaPrincipal));

						JRPdfExporter exporter = new JRPdfExporter();
						exporter.setExporterInput(new SimpleExporterInput(jasperPrint));

						exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(configuracion.getRutaDescarga()
								+ Constantes.SLASH
								+ nombreBoletaAppMovil(empleado.getEMPCODTRA(), proceso, configuracion.getPeriodo())));

						if (configuracion.isCifrado()) {
							SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
							configuration.setEncrypted(true);
							configuration.set128BitKey(true);
							configuration.setUserPassword(empleado.getEMPNRODOCID());
							configuration.setOwnerPassword(empleado.getEMPNRODOCID());
							configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
							exporter.setConfiguration(configuration);
						}

						exporter.exportReport();

						cantidadEmpleados++;

						if (cantidadEmpleados >= cantidadImprimir) {
							log.info("Se est�n descargando " + cantidadImprimir + " boletas...");
							cantidadImprimir += 10;
						}

					} catch (JRException e) {
						log.error("Error en conexi�n BD", e);
					}
				}
				log.info("Se han descargado " + cantidadEmpleados + " boletas del proceso " + proceso);
				cantidaTotal += cantidadEmpleados;
			}

		} catch (SQLException e) {
			log.error("Error en conexi�n BD", e);
		} finally {
			DBConnection.cerrarConexion();
		}

		return cantidaTotal;
	}

	private String nombreBoletaAppMovil(String codigoEmpleado, String codigoProceso, String periodo) {
		return codigoEmpleado + Constantes.GUION_BAJO + periodo + Constantes.EXTENSION_PDF;
	}

	@Override
	public int envioMasivoBoletas(Configuracion configuracion) {
		int cantidadEnviados = 0;
		Properties companiaProperties = FileReader.readProperties(ConstantesCompania.PROPERTIES_INTRALOT);

		try {
			DBConnection.setConexion(companiaProperties);

			CompaniaBean compania = boletaDAO.obtenerCompania(configuracion.getCompania());

			List<BeanHR_EMPLEADO> empleados = boletaDAO.listarEmpleadoXProceso(compania.getEBCODCIA(),
					compania.getEBCODSUC(), Constantes.ESTADO_PROCESADO, configuracion.getPeriodo(),
					configuracion.getProceso());

			emailService
					.setEmailProperties(FileReader.readProperties(companiaProperties.getProperty("email_property")));
			for (BeanHR_EMPLEADO empleado : empleados) {
				String html = FileReader.readTextFile(companiaProperties.getProperty("email_boleta"));
				html = html.replace("${nombre}", empleado.getEMPNOMBRE());
				html = html.replace("${tipoDocumento}", "boleta de pago");
				html = html.replace("${mes}", empleado.getMES());
				html = html.replace("${ano}", empleado.getANIO());
				
				if (emailService.enviarBoleta("smuroy@targethr.com.pe", "", html)) // empleado.getEMPEMAIL()
					cantidadEnviados++;
			}

		} catch (SQLException e) {
			log.error("Error en conexi�n BD", e);
		} finally {
			DBConnection.cerrarConexion();
		}

		return cantidadEnviados;
	}
}
