package service.impl;

import java.util.Properties;

import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.email.EmailPopulatingBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.simplejavamail.util.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import service.EmailService;
import utils.FileReader;

public class EmailServiceImpl implements EmailService {
	private static Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Override
	public boolean enviarBoleta(String to, String subject, String body, String... rutasFile) {
		boolean resultado = false;

		try {
			EmailPopulatingBuilder emailBuilder = EmailBuilder.startingBlank();
			emailBuilder.to(to);
			emailBuilder.withSubject(subject);
			emailBuilder.withHTMLText(body);

			boolean existeArchivo = false;
			for (String ruta : rutasFile) {

				byte[] bytefile = FileReader.readBytesFromFile(ruta);

				if (bytefile.length > 0) {
					emailBuilder.withAttachment("Boleta Pago", bytefile, "application/pdf");
					existeArchivo = true;
				} else {
					log.error("No existe archivo para " + to);
				}
			}

			if (existeArchivo) {
				MailerBuilder.buildMailer().sendMail(emailBuilder.buildEmail());
				resultado = true;
			}

		} catch (Exception e) {
			log.error(e.toString());
		}
		return resultado;
	}

	@Override
	public void setEmailProperties(Properties properties) {
		ConfigLoader.loadProperties(properties, false);
	}

}
