package service.impl;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfWriter;

import constants.Constantes;
import constants.ConstantesCompania;
import dao.impl.UniconBoletaDAO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import service.BoletaService;
import service.EmailService;
import utils.DBConnection;
import utils.FileReader;
import utils.Formatter;

public class UniconBoletaService implements BoletaService {
	private static Logger log = LoggerFactory.getLogger(UniconBoletaService.class);

	private static final String CONCEPTO_ESSALUD = "3010";
	private static final String CONCEPTO_SENATI = "3030";

	private UniconBoletaDAO boletaDAO = new UniconBoletaDAO();
	private EmailService emailService = new EmailServiceImpl();

	@Override
	public int descargaMasivaBoletas(Configuracion configuracion) {
		Properties companiaProperties = FileReader.readProperties(ConstantesCompania.PROPERTIES_UNICON);
		int cantidaTotal = 0;

		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
		decimalFormatSymbols.setDecimalSeparator('.');
		decimalFormatSymbols.setGroupingSeparator(',');
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);

		String[] procesos = configuracion.getProceso().split(Constantes.COMA);

		try {
			DBConnection.setConexion(companiaProperties);

			CompaniaBean compania = boletaDAO.obtenerCompania(configuracion.getCompania());

			for (String proceso : procesos) {
				int cantidadEmpleados = 0;
				int cantidadImprimir = 10;

				proceso = proceso.trim();
				List<BeanHR_EMPLEADO> empleados = boletaDAO.listarEmpleadoXProceso(compania.getEBCODCIA(),
						compania.getEBCODSUC(), Constantes.ESTADO_PROCESADO, configuracion.getPeriodo(), proceso);

				for (BeanHR_EMPLEADO empleado : empleados) {
					List<ReporteBoletaPago> boletaPrincipal = boletaDAO.listarPrincipalBoleta(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> haberes = boletaDAO.listarHaberes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> descuentos = boletaDAO.listarDescuentos(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> aportes = boletaDAO.listarAportes(compania.getEBCODCIA(),
							compania.getEBCODSUC(), proceso, configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), Constantes.CUALQUIERA, Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<ReporteSubreporte> montosAdicionales = boletaDAO.listarMontosAdicionales(
							compania.getEBCODCIA(), compania.getEBCODSUC(), proceso, configuracion.getPeriodo(),
							configuracion.getPeriodo(), empleado.getEMPCODTRA(), Constantes.CUALQUIERA,
							Constantes.CUALQUIERA, Constantes.CUALQUIERA, Constantes.CUALQUIERA);

					List<VacacionesCabBean> vacaciones = boletaDAO.listarVacaciones(compania.getEBCODCIA(),
							compania.getEBCODSUC(), configuracion.getPeriodo(), configuracion.getPeriodo(),
							empleado.getEMPCODTRA(), proceso);

					List<SubreporteDiasLaborados> diasLaborados = boletaDAO.listarDiasTrabajados(compania.getEBCODCIA(),
							compania.getEBCODSUC(), configuracion.getPeriodo(), proceso, empleado.getEMPCODTRA());

					/*List<SubreporteAsistencias> asistencias = boletaDAO.listarAsistencias(compania.getEBCODCIA(),
							compania.getEBCODSUC(), "00046674", "2019",6);*/

					for (ReporteBoletaPago reporteBoleta : boletaPrincipal) {
						reporteBoleta.setNombre(reporteBoleta.getNombreCompleto());

						reporteBoleta.setListaRemuneracion((ArrayList<ReporteSubreporte>) haberes);
						reporteBoleta.setListaDeduccion((ArrayList<ReporteSubreporte>) descuentos);
						reporteBoleta.setListaAportacion((ArrayList<ReporteSubreporte>) aportes);
						reporteBoleta.setListaMontosAdicionales((ArrayList<ReporteSubreporte>) montosAdicionales);
						reporteBoleta.setListaVacaciones((ArrayList<VacacionesCabBean>) vacaciones);
						reporteBoleta.setListaDiasLaborados((ArrayList<SubreporteDiasLaborados>) diasLaborados);

						List<SubreporteAsistencias> asistencias = boletaDAO.listarAsistencias(compania.getEBCODCIA(),
								compania.getEBCODSUC(), reporteBoleta.getCodtra(), reporteBoleta.getAnio_periodo(),Integer.valueOf(reporteBoleta.getMesNumero()));
						log.info("codtra: "+reporteBoleta.getCodtra()+" ANIO Y MES "+reporteBoleta.getAnio_periodo()+" "+
								Integer.valueOf(reporteBoleta.getMesNumero())+" cantidad: "+asistencias.size());
						reporteBoleta.setListaAsistencias((ArrayList<SubreporteAsistencias>) asistencias);



						double gananciaTotal = 0;
						double deduccionTotal = 0;

						for (ReporteSubreporte haber : haberes) {
							double monto = 0;
							try {
								monto = decimalFormat.parse(haber.getMonto()).doubleValue();
							} catch (ParseException e) {
								log.error("Error en n�mero", e);
							}
							gananciaTotal += monto;
						}

						for (ReporteSubreporte descuento : descuentos) {
							double monto = 0;
							try {
								monto = decimalFormat.parse(descuento.getMonto()).doubleValue();
							} catch (ParseException e) {
								log.error("Error en n�mero", e);
							}
							deduccionTotal += monto;
						}

						reporteBoleta.setMontoEssalud("0");
						reporteBoleta.setMontoSenati("0");
						for (ReporteSubreporte aporte : aportes) {
							if (CONCEPTO_ESSALUD.equals(aporte.getCodcon().trim())) {
								reporteBoleta.setMontoEssalud(aporte.getMonto());
							}

							if (CONCEPTO_SENATI.equals(aporte.getCodcon().trim())) {
								reporteBoleta.setMontoSenati(aporte.getMonto());
							}
						}

						reporteBoleta.setGananciaTotal(decimalFormat.format(gananciaTotal));
						reporteBoleta.setDeduccionTotal(decimalFormat.format(deduccionTotal));

					}

					Map<String, Object> parametros = new HashMap<>();
					parametros.put("SUBREPORT_DIR",
							getClass().getResource(companiaProperties.getProperty("ruta_reporte")).getPath());
					parametros.put("ruta", getClass().getResource("/").getPath());

					if(compania.getEBCODCIA().equals("061")){

						parametros.put("firma",getClass().getResource(companiaProperties.getProperty("firma_unicon")).getPath());
						parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo_unicon")).getPath());
					}else if(compania.getEBCODCIA().equals("062")){

						parametros.put("firma",getClass().getResource(companiaProperties.getProperty("firma_concremax")).getPath());
						parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo_concremax")).getPath());
					}else if(compania.getEBCODCIA().equals("063")){

						parametros.put("firma",getClass().getResource(companiaProperties.getProperty("firma_entrepisos")).getPath());
						parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo_entrepisos")).getPath());
					}else if(compania.getEBCODCIA().equals("064")){

						parametros.put("firma",getClass().getResource(companiaProperties.getProperty("firma_agrecom")).getPath());
						parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo_agrecom")).getPath());
					}else if(compania.getEBCODCIA().equals("065")){

						parametros.put("firma",getClass().getResource(companiaProperties.getProperty("firma_basf")).getPath());
						parametros.put("logo", getClass().getResource(companiaProperties.getProperty("logo_basf")).getPath());
					}

					parametros.put("p_cnn", DBConnection.conexion);

					try {

						String rutaReporte = companiaProperties.getProperty("ruta_reporte")
								+ companiaProperties.getProperty("reporte");

						JasperReport reporte = (JasperReport) JRLoader
								.loadObject(getClass().getResource((rutaReporte)));

						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros,
								new JRBeanCollectionDataSource(boletaPrincipal));

						JRPdfExporter exporter = new JRPdfExporter();
						exporter.setExporterInput(new SimpleExporterInput(jasperPrint));

						exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
								configuracion.getRutaDescarga() + Constantes.SLASH + Formatter.nombreBoletaAppMovil(
										empleado.getEMPCODTRA(), proceso, configuracion.getPeriodo()+Constantes.GUION_BAJO+proceso)));

						if (configuracion.isCifrado()) {
							SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
							configuration.setEncrypted(true);
							configuration.set128BitKey(true);
							configuration.setUserPassword(empleado.getEMPNRODOCID());
							configuration.setOwnerPassword(empleado.getEMPNRODOCID());
							configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
							exporter.setConfiguration(configuration);
						}

						exporter.exportReport();

						cantidadEmpleados++;

						if (cantidadEmpleados >= cantidadImprimir) {
							log.info("Se est�n descargando " + cantidadImprimir + " boletas...");
							cantidadImprimir += 10;
						}

					} catch (JRException e) {
						log.error("Error en conexi�n BD", e);
					}
				}
				log.info("Se han descargado " + cantidadEmpleados + " boletas del proceso " + proceso);
				cantidaTotal += cantidadEmpleados;
			}

		} catch (SQLException e) {
			log.error("Error en conexi�n BD", e.getMessage());
		} finally {
			DBConnection.cerrarConexion();
		}

		return cantidaTotal;
	}

	@Override
	public int envioMasivoBoletas(Configuracion configuracion) {
		int cantidadEnviados = 0;
		Properties companiaProperties = FileReader.readProperties(ConstantesCompania.PROPERTIES_UNICON);

		try {
			DBConnection.setConexion(companiaProperties);

			CompaniaBean compania = boletaDAO.obtenerCompania(configuracion.getCompania());

			List<BeanHR_EMPLEADO> empleados = boletaDAO.listarEmpleadoXProceso(compania.getEBCODCIA(),
					compania.getEBCODSUC(), Constantes.ESTADO_PROCESADO, configuracion.getPeriodo(),
					configuracion.getProceso());

			String rutaEmailProperty = companiaProperties.getProperty("email_property");

			emailService.setEmailProperties(FileReader.readProperties(rutaEmailProperty));

			for (BeanHR_EMPLEADO empleado : empleados) {
				String html = FileReader.readTextFile(companiaProperties.getProperty("email_boleta"));

				String mes = Formatter.monthToText(configuracion.getPeriodo().substring(4, 6));
				String ano = configuracion.getPeriodo().substring(0, 4);

				String asunto = "Boleta de pago - " + mes + " - " + ano;

				String rutaFile = configuracion.getRutaDescarga() + Constantes.SLASH + Formatter.nombreBoletaAppMovil(
						empleado.getEMPCODTRA(), configuracion.getProceso(), configuracion.getPeriodo());

				html = html.replace("${mes}", mes);
				html = html.replace("${ano}", ano);

				if (emailService.enviarBoleta("rmendoza@targethr.com.pe", asunto, html, rutaFile)) // empleado.getEMPEMAIL()
					cantidadEnviados++;
			}

		} catch (SQLException e) {
			log.error("Error en conexi�n BD", e);
		} finally {
			DBConnection.cerrarConexion();
		}

		return cantidadEnviados;
	};
}
