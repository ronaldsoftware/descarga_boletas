package service;

import model.Configuracion;

public interface BoletaService {

	public int descargaMasivaBoletas(Configuracion configuracion);

	public int envioMasivoBoletas(Configuracion configuracion);

}
