package component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import constants.ConstantesCompania;
import model.Configuracion;
import service.BoletaService;
import service.impl.ComsatelBoletaService;
import service.impl.IntralotBoletaService;
import service.impl.PuratosBoletaService;
import service.impl.UniconBoletaService;

public class PanelPantallaInicial {
	private static Logger log = Logger.getLogger(PanelPantallaInicial.class);

	private final static int POSICION_X = 40;
	private final static int POSICION_Y = 40;
	private final static int LABEL_ANCHO = 100;
	private final static int LABEL_ALTO = 30;

	private JPanel pantalla;

	public PanelPantallaInicial() {
		BasicConfigurator.configure();
		JLabel lblCompania = new JLabel("Compa�ia:");
		lblCompania.setBounds(POSICION_X, POSICION_Y * 1, LABEL_ANCHO, LABEL_ALTO);
		JLabel lblProceso = new JLabel("Proceso:");
		lblProceso.setBounds(POSICION_X, POSICION_Y * 2, LABEL_ANCHO, LABEL_ALTO);
		JLabel lblPeriodo = new JLabel("Periodo:");
		lblPeriodo.setBounds(POSICION_X, POSICION_Y * 3, LABEL_ANCHO, LABEL_ALTO);
		JLabel lblRutaDescarga = new JLabel("Ruta descarga:");
		lblRutaDescarga.setBounds(POSICION_X, POSICION_Y * 4, LABEL_ANCHO, LABEL_ALTO);

		final JComboBox<ComboItem> cboCompania = new JComboBox<ComboItem>();
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_INTRALOT, ConstantesCompania.NOMBRE_INTRALOT));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_COMSATEL, ConstantesCompania.NOMBRE_COMSATEL));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_PURATOS, ConstantesCompania.NOMBRE_PURATOS));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_UNICON, ConstantesCompania.NOMBRE_UNICON));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_CONCREMAX, ConstantesCompania.NOMBRE_CONCREMAX));
		cboCompania
				.addItem(new ComboItem(ConstantesCompania.COMPANIA_ENTREPISOS, ConstantesCompania.NOMBRE_ENTREPISOS));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_AGRECOM, ConstantesCompania.NOMBRE_AGRECOM));
		cboCompania.addItem(new ComboItem(ConstantesCompania.COMPANIA_BASF, ConstantesCompania.NOMBRE_BASF));
		cboCompania.setSelectedIndex(3);

		cboCompania.setBounds(POSICION_X * 4, POSICION_Y * 1, LABEL_ANCHO * 2, LABEL_ALTO);

		final JTextField txtProceso = new JTextField();
		txtProceso.setBounds(POSICION_X * 4, POSICION_Y * 2, LABEL_ANCHO / 2, LABEL_ALTO);
		txtProceso.setText("02");
		final JTextField txtPeriodo = new JTextField();
		txtPeriodo.setBounds(POSICION_X * 4, POSICION_Y * 3, LABEL_ANCHO, LABEL_ALTO);
		txtPeriodo.setText("201906");

		final JTextField txtRutaDescarga = new JTextField();
		txtRutaDescarga.setBounds(POSICION_X * 4, POSICION_Y * 4, LABEL_ANCHO * 4, LABEL_ALTO);
		txtRutaDescarga.setText("C:\\BOLETAS\\UNICON2");

		JButton btnDescargar = new JButton("Descargar Boletas");
		btnDescargar.setBounds(POSICION_X * 3, POSICION_Y * 5, LABEL_ANCHO * 3, LABEL_ALTO);
		btnDescargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComboItem item = (ComboItem) cboCompania.getSelectedItem();

				Configuracion configuracion = new Configuracion();
				configuracion.setCompania(item.getValue());
				configuracion.setProceso(txtProceso.getText());
				configuracion.setPeriodo(txtPeriodo.getText());
				configuracion.setRutaDescarga(txtRutaDescarga.getText());

				descargarBoletas(configuracion);
			}
		});

		JButton btnEnviarCorreos = new JButton("Enviar Boletas");
		btnEnviarCorreos.setBounds(POSICION_X * 3, POSICION_Y * 6, LABEL_ANCHO * 3, LABEL_ALTO);
		btnEnviarCorreos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComboItem item = (ComboItem) cboCompania.getSelectedItem();

				Configuracion configuracion = new Configuracion();
				configuracion.setCompania(item.getValue());
				configuracion.setProceso(txtProceso.getText());
				configuracion.setPeriodo(txtPeriodo.getText());
				configuracion.setRutaDescarga(txtRutaDescarga.getText());

				enviarBoletas(configuracion);
			}
		});

		JTextArea txtLog = new JTextArea();
		txtLog.setBounds(POSICION_X, POSICION_Y * 6, LABEL_ANCHO * 5, LABEL_ALTO * 4);
		txtLog.setEditable(false);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setVisible(true);
		panel.add(lblCompania);
		panel.add(cboCompania);
		panel.add(lblProceso);
		panel.add(txtProceso);
		panel.add(lblPeriodo);
		panel.add(txtPeriodo);
		panel.add(lblRutaDescarga);
		panel.add(txtRutaDescarga);
		panel.add(btnDescargar);
		panel.add(btnEnviarCorreos);
//		panel.add(txtLog);

		pantalla = panel;
	}

	public JPanel getPantalla() {
		return pantalla;
	}

	public void setPantalla(JPanel pantalla) {
		this.pantalla = pantalla;
	}

	public static void descargarBoletas(Configuracion configuracion) {
		log.info("Iniciando proceso de descarga de boletas...");
		BoletaService boletaService = configuracionXCompania(configuracion);

		int cantidadProcesadas = boletaService.descargaMasivaBoletas(configuracion);

		JOptionPane.showMessageDialog(null, "Se decargaron " + cantidadProcesadas + " boletas.");
	}

	public static void enviarBoletas(Configuracion configuracion) {
		log.info("Iniciando proceso de env�o de boletas...");
		BoletaService boletaService = configuracionXCompania(configuracion);

		int cantidadProcesadas = boletaService.envioMasivoBoletas(configuracion);

		JOptionPane.showMessageDialog(null, "Se enviaron " + cantidadProcesadas + " boletas.");
	}

	public static BoletaService configuracionXCompania(Configuracion configuracion) {
		BoletaService boletaService = null;

		switch (configuracion.getCompania()) {
		case ConstantesCompania.COMPANIA_COMSATEL:
			boletaService = new ComsatelBoletaService();
			configuracion.setCifrado(false);
			configuracion.setBoletaDoble(false);
			break;
		case ConstantesCompania.COMPANIA_PURATOS:
			boletaService = new PuratosBoletaService();
			configuracion.setCifrado(false);
			configuracion.setBoletaDoble(false);
			break;
		case ConstantesCompania.COMPANIA_UNICON:
		case ConstantesCompania.COMPANIA_CONCREMAX:
		case ConstantesCompania.COMPANIA_ENTREPISOS:
		case ConstantesCompania.COMPANIA_AGRECOM:
		case ConstantesCompania.COMPANIA_BASF:
			boletaService = new UniconBoletaService();
			configuracion.setCifrado(false);
			configuracion.setBoletaDoble(false);
			break;
		case ConstantesCompania.COMPANIA_INTRALOT:
			boletaService = new IntralotBoletaService();
			configuracion.setCifrado(false);
			configuracion.setBoletaDoble(false);
			break;
		}

		return boletaService;
	}

}
