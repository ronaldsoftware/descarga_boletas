package model;
import java.io.Serializable;

public class BeanEBCOMPANIABOLETA implements Serializable {

	private static final long serialVersionUID = 1L;
	private String codcia;
	private String codsuc;
	private String tipobol;
	private String rutafir;
	private String rutalogo;
	private String rutabol;
	private String rutasub;
	private String nombre;
	
	public String getCodcia() {
		return codcia;
	}
	public void setCodcia(String codcia) {
		this.codcia = codcia;
	}
	public String getCodsuc() {
		return codsuc;
	}
	public void setCodsuc(String codsuc) {
		this.codsuc = codsuc;
	}
	public String getTipobol() {
		return tipobol;
	}
	public void setTipobol(String tipobol) {
		this.tipobol = tipobol;
	}
	public String getRutafir() {
		return rutafir;
	}
	public void setRutafir(String rutafir) {
		this.rutafir = rutafir;
	}
	public String getRutalogo() {
		return rutalogo;
	}
	public void setRutalogo(String rutalogo) {
		this.rutalogo = rutalogo;
	}
	public String getRutabol() {
		return rutabol;
	}
	public void setRutabol(String rutabol) {
		this.rutabol = rutabol;
	}
	public String getRutasub() {
		return rutasub;
	}
	public void setRutasub(String rutasub) {
		this.rutasub = rutasub;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	 
	
}
