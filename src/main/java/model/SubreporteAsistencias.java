package model;

import java.io.Serializable;

public class SubreporteAsistencias implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pfecha, inicio,fin ,entrada,salida,periodo,empresa,ruc,trabajador,dni,codigo;
    private double pdsccomp,phorasferiado,pfaltasinjustif,pfaltasjustifi,pdmedsubslicpat,pvacaciones,phorastotales,phorascontrol,pdescprovin,phextra100;
    private double ptotdomini,totphorasferiado,totphorascontrol,phe50,ph100;

    public double getTotphorascontrol() {
        return totphorascontrol;
    }

    public void setTotphorascontrol(double totphorascontrol) {
        this.totphorascontrol = totphorascontrol;
    }

    public double getPhe50() {
        return phe50;
    }

    public void setPhe50(double phe50) {
        this.phe50 = phe50;
    }

    public double getPh100() {
        return ph100;
    }

    public void setPh100(double ph100) {
        this.ph100 = ph100;
    }

    public double getPtotdomini() {
        return ptotdomini;
    }

    public void setPtotdomini(double ptotdomini) {
        this.ptotdomini = ptotdomini;
    }

    public double getTotphorasferiado() {
        return totphorasferiado;
    }

    public void setTotphorasferiado(double totphorasferiado) {
        this.totphorasferiado = totphorasferiado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(String trabajador) {
        this.trabajador = trabajador;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPfecha() {
        return pfecha;
    }

    public void setPfecha(String pfecha) {
        this.pfecha = pfecha;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public double getPdsccomp() {
        return pdsccomp;
    }

    public void setPdsccomp(double pdsccomp) {
        this.pdsccomp = pdsccomp;
    }

    public double getPhorasferiado() {
        return phorasferiado;
    }

    public void setPhorasferiado(double phorasferiado) {
        this.phorasferiado = phorasferiado;
    }

    public double getPfaltasinjustif() {
        return pfaltasinjustif;
    }

    public void setPfaltasinjustif(double pfaltasinjustif) {
        this.pfaltasinjustif = pfaltasinjustif;
    }

    public double getPfaltasjustifi() {
        return pfaltasjustifi;
    }

    public void setPfaltasjustifi(double pfaltasjustifi) {
        this.pfaltasjustifi = pfaltasjustifi;
    }

    public double getPdmedsubslicpat() {
        return pdmedsubslicpat;
    }

    public void setPdmedsubslicpat(double pdmedsubslicpat) {
        this.pdmedsubslicpat = pdmedsubslicpat;
    }

    public double getPvacaciones() {
        return pvacaciones;
    }

    public void setPvacaciones(double pvacaciones) {
        this.pvacaciones = pvacaciones;
    }

    public double getPhorastotales() {
        return phorastotales;
    }

    public void setPhorastotales(double phorastotales) {
        this.phorastotales = phorastotales;
    }

    public double getPhorascontrol() {
        return phorascontrol;
    }

    public void setPhorascontrol(double phorascontrol) {
        this.phorascontrol = phorascontrol;
    }

    public double getPdescprovin() {
        return pdescprovin;
    }

    public void setPdescprovin(double pdescprovin) {
        this.pdescprovin = pdescprovin;
    }

    public double getPhextra100() {
        return phextra100;
    }

    public void setPhextra100(double phextra100) {
        this.phextra100 = phextra100;
    }
}
