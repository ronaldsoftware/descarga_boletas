package model;

import java.io.Serializable;

public class VacacionesCabBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String VACCODCIA, VACCODSUC, VACCODTRA, VACNROCTL, VACTIPVAC,VACDESTIPVAC,
			VACFECINI, VACDIASPRO, VACFECFIN, VACCODPRO, VACPERPRO, VACFECCREA,
			VACUSUCREA, VACUSUMOD, VACFECMOD, VACFLGEST, VACFLGPRO, VACGLOSA,
			VACDIASGOZ, VACPERVA1, VACPERVA2, aVACDESTRA, AP1, AP2, AP3, AP4,
			AP5,VACTIPVAR,VACCORREL;

	
	
	
	public String getVACDIASGOZ() {
		return VACDIASGOZ;
	}

	public void setVACDIASGOZ(String vACDIASGOZ) {
		VACDIASGOZ = vACDIASGOZ;
	}

	public String getVACPERVA1() {
		return VACPERVA1;
	}

	public void setVACPERVA1(String vACPERVA1) {
		VACPERVA1 = vACPERVA1;
	}

	public String getVACPERVA2() {
		return VACPERVA2;
	}

	public void setVACPERVA2(String vACPERVA2) {
		VACPERVA2 = vACPERVA2;
	}

	public String getaVACDESTRA() {
		return aVACDESTRA;
	}

	public String getAP2() {
		return AP2;
	}

	public void setAP2(String aP2) {
		AP2 = aP2;
	}

	public String getAP3() {
		return AP3;
	}

	public void setAP3(String aP3) {
		AP3 = aP3;
	}

	public String getAP4() {
		return AP4;
	}

	public void setAP4(String aP4) {
		AP4 = aP4;
	}

	public String getAP5() {
		return AP5;
	}

	public void setAP5(String aP5) {
		AP5 = aP5;
	}

	public void setaVACDESTRA(String aVACDESTRA) {
		this.aVACDESTRA = aVACDESTRA;
	}

	public String getVACCODCIA() {
		return VACCODCIA;
	}

	public void setVACCODCIA(String vACCODCIA) {
		VACCODCIA = vACCODCIA;
	}

	public String getVACCODSUC() {
		return VACCODSUC;
	}

	public void setVACCODSUC(String vACCODSUC) {
		VACCODSUC = vACCODSUC;
	}

	public String getVACCODTRA() {
		return VACCODTRA;
	}

	public void setVACCODTRA(String vACCODTRA) {
		VACCODTRA = vACCODTRA;
	}

	public String getVACNROCTL() {
		return VACNROCTL;
	}

	public void setVACNROCTL(String vACNROCTL) {
		VACNROCTL = vACNROCTL;
	}

	public String getVACTIPVAC() {
		return VACTIPVAC;
	}

	public void setVACTIPVAC(String vACTIPVAC) {
		VACTIPVAC = vACTIPVAC;
	}

	public String getVACFECINI() {
		return VACFECINI;
	}

	public void setVACFECINI(String vACFECINI) {
		VACFECINI = vACFECINI;
	}

	public String getVACDIASPRO() {
		return VACDIASPRO;
	}

	public void setVACDIASPRO(String vACDIASPRO) {
		VACDIASPRO = vACDIASPRO;
	}

	public String getVACFECFIN() {
		return VACFECFIN;
	}

	public void setVACFECFIN(String vACFECFIN) {
		VACFECFIN = vACFECFIN;
	}

	public String getVACCODPRO() {
		return VACCODPRO;
	}

	public void setVACCODPRO(String vACCODPRO) {
		VACCODPRO = vACCODPRO;
	}

	public String getVACPERPRO() {
		return VACPERPRO;
	}

	public void setVACPERPRO(String vACPERPRO) {
		VACPERPRO = vACPERPRO;
	}

	public String getVACFECCREA() {
		return VACFECCREA;
	}

	public void setVACFECCREA(String vACFECCREA) {
		VACFECCREA = vACFECCREA;
	}

	public String getVACUSUCREA() {
		return VACUSUCREA;
	}

	public void setVACUSUCREA(String vACUSUCREA) {
		VACUSUCREA = vACUSUCREA;
	}

	public String getVACUSUMOD() {
		return VACUSUMOD;
	}

	public void setVACUSUMOD(String vACUSUMOD) {
		VACUSUMOD = vACUSUMOD;
	}

	public String getVACFECMOD() {
		return VACFECMOD;
	}

	public void setVACFECMOD(String vACFECMOD) {
		VACFECMOD = vACFECMOD;
	}

	public String getVACFLGEST() {
		return VACFLGEST;
	}

	public void setVACFLGEST(String vACFLGEST) {
		VACFLGEST = vACFLGEST;
	}

	public String getVACFLGPRO() {
		return VACFLGPRO;
	}

	public void setVACFLGPRO(String vACFLGPRO) {
		VACFLGPRO = vACFLGPRO;
	}

	public String getVACGLOSA() {
		return VACGLOSA;
	}

	public void setVACGLOSA(String vACGLOSA) {
		VACGLOSA = vACGLOSA;
	}

	public String getAP1() {
		return AP1;
	}

	public void setAP1(String aP1) {
		AP1 = aP1;
	}

	public String getVACTIPVAR() {
		return VACTIPVAR;
	}

	public void setVACTIPVAR(String vACTIPVAR) {
		VACTIPVAR = vACTIPVAR;
	}

	public String getVACCORREL() {
		return VACCORREL;
	}

	public void setVACCORREL(String vACCORREL) {
		VACCORREL = vACCORREL;
	}

	public String getVACDESTIPVAC() {
		return VACDESTIPVAC;
	}

	public void setVACDESTIPVAC(String vACDESTIPVAC) {
		VACDESTIPVAC = vACDESTIPVAC;
	}

	
}
