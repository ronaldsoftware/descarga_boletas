package model;

import java.io.Serializable;

public class CompaniaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String EBCODCIA, EBCODSUC, EBDESCIA, EBDESSUC, EBNRORUC, EBDESCIAC,
			EBDESSUCC,EBACTIVPDT,EBCODMANGO,EBDIRECCION, EBREPRESENTANTE, EBDNIREPRE, EBCARGOREP,
			EBREPRESENTANTE1, EBDNIREPRE1, EBCARGOREP1,EBTELF1, EBTELF2, EBTELF3, EBEMAIL, EBWEBSITE, EBLOGO, EBCODPAIS,
			EBLONGITUD, EBLATITUD, EBUSUCREA, EBFECCREA,
			EBUSUMOD, EBFECMOD,EBREPRESENTANTEFIRMA,EBREPRESENTANTEFIRMA2,EBREPRESENTANTE2,EBDNIREPRE2,EBCARGOREP2;

	
	
	
	public String getEBREPRESENTANTEFIRMA2() {
		return EBREPRESENTANTEFIRMA2;
	}

	public void setEBREPRESENTANTEFIRMA2(String eBREPRESENTANTEFIRMA2) {
		EBREPRESENTANTEFIRMA2 = eBREPRESENTANTEFIRMA2;
	}

	public String getEBREPRESENTANTE2() {
		return EBREPRESENTANTE2;
	}

	public void setEBREPRESENTANTE2(String eBREPRESENTANTE2) {
		EBREPRESENTANTE2 = eBREPRESENTANTE2;
	}

	public String getEBDNIREPRE2() {
		return EBDNIREPRE2;
	}

	public void setEBDNIREPRE2(String eBDNIREPRE2) {
		EBDNIREPRE2 = eBDNIREPRE2;
	}

	public String getEBCARGOREP2() {
		return EBCARGOREP2;
	}

	public void setEBCARGOREP2(String eBCARGOREP2) {
		EBCARGOREP2 = eBCARGOREP2;
	}

	public String getEBREPRESENTANTEFIRMA() {
		return EBREPRESENTANTEFIRMA;
	}

	public void setEBREPRESENTANTEFIRMA(String eBREPRESENTANTEFIRMA) {
		EBREPRESENTANTEFIRMA = eBREPRESENTANTEFIRMA;
	}

	public String getEBUSUCREA() {
		return EBUSUCREA;
	}

	public void setEBUSUCREA(String eBUSUCREA) {
		EBUSUCREA = eBUSUCREA;
	}

	public String getEBFECCREA() {
		return EBFECCREA;
	}

	public void setEBFECCREA(String eBFECCREA) {
		EBFECCREA = eBFECCREA;
	}

	public String getEBUSUMOD() {
		return EBUSUMOD;
	}

	public void setEBUSUMOD(String eBUSUMOD) {
		EBUSUMOD = eBUSUMOD;
	}

	public String getEBFECMOD() {
		return EBFECMOD;
	}

	public void setEBFECMOD(String eBFECMOD) {
		EBFECMOD = eBFECMOD;
	}

	public String getEBCODCIA() {
		return EBCODCIA;
	}

	public void setEBCODCIA(String eBCODCIA) {
		EBCODCIA = eBCODCIA;
	}

	public String getEBCODSUC() {
		return EBCODSUC;
	}

	public void setEBCODSUC(String eBCODSUC) {
		EBCODSUC = eBCODSUC;
	}

	public String getEBDESCIA() {
		return EBDESCIA;
	}

	public void setEBDESCIA(String eBDESCIA) {
		EBDESCIA = eBDESCIA;
	}

	public String getEBDESSUC() {
		return EBDESSUC;
	}

	public void setEBDESSUC(String eBDESSUC) {
		EBDESSUC = eBDESSUC;
	}

	public String getEBNRORUC() {
		return EBNRORUC;
	}

	public void setEBNRORUC(String eBNRORUC) {
		EBNRORUC = eBNRORUC;
	}

	public String getEBDESCIAC() {
		return EBDESCIAC;
	}

	public void setEBDESCIAC(String eBDESCIAC) {
		EBDESCIAC = eBDESCIAC;
	}

	public String getEBDESSUCC() {
		return EBDESSUCC;
	}

	public void setEBDESSUCC(String eBDESSUCC) {
		EBDESSUCC = eBDESSUCC;
	}

	public String getEBDIRECCION() {
		return EBDIRECCION;
	}

	public void setEBDIRECCION(String eBDIRECCION) {
		EBDIRECCION = eBDIRECCION;
	}

	public String getEBREPRESENTANTE() {
		return EBREPRESENTANTE;
	}

	public void setEBREPRESENTANTE(String eBREPRESENTANTE) {
		EBREPRESENTANTE = eBREPRESENTANTE;
	}

	public String getEBDNIREPRE() {
		return EBDNIREPRE;
	}

	public void setEBDNIREPRE(String eBDNIREPRE) {
		EBDNIREPRE = eBDNIREPRE;
	}

	public String getEBCARGOREP() {
		return EBCARGOREP;
	}

	public void setEBCARGOREP(String eBCARGOREP) {
		EBCARGOREP = eBCARGOREP;
	}

	public String getEBTELF1() {
		return EBTELF1;
	}

	public void setEBTELF1(String eBTELF1) {
		EBTELF1 = eBTELF1;
	}

	public String getEBTELF2() {
		return EBTELF2;
	}

	public void setEBTELF2(String eBTELF2) {
		EBTELF2 = eBTELF2;
	}

	public String getEBTELF3() {
		return EBTELF3;
	}

	public void setEBTELF3(String eBTELF3) {
		EBTELF3 = eBTELF3;
	}

	public String getEBEMAIL() {
		return EBEMAIL;
	}

	public void setEBEMAIL(String eBEMAIL) {
		EBEMAIL = eBEMAIL;
	}

	public String getEBWEBSITE() {
		return EBWEBSITE;
	}

	public void setEBWEBSITE(String eBWEBSITE) {
		EBWEBSITE = eBWEBSITE;
	}

	public String getEBLOGO() {
		return EBLOGO;
	}

	public void setEBLOGO(String eBLOGO) {
		EBLOGO = eBLOGO;
	}

	public String getEBCODPAIS() {
		return EBCODPAIS;
	}

	public void setEBCODPAIS(String eBCODPAIS) {
		EBCODPAIS = eBCODPAIS;
	}

	public String getEBLONGITUD() {
		return EBLONGITUD;
	}

	public void setEBLONGITUD(String eBLONGITUD) {
		EBLONGITUD = eBLONGITUD;
	}

	public String getEBLATITUD() {
		return EBLATITUD;
	}

	public void setEBLATITUD(String eBLATITUD) {
		EBLATITUD = eBLATITUD;
	}

	public String getEBACTIVPDT() {
		return EBACTIVPDT;
	}

	public void setEBACTIVPDT(String eBACTIVPDT) {
		EBACTIVPDT = eBACTIVPDT;
	}

	public String getEBCODMANGO() {
		return EBCODMANGO;
	}

	public void setEBCODMANGO(String eBCODMANGO) {
		EBCODMANGO = eBCODMANGO;
	}

	public String getEBREPRESENTANTE1() {
		return EBREPRESENTANTE1;
	}

	public void setEBREPRESENTANTE1(String eBREPRESENTANTE1) {
		EBREPRESENTANTE1 = eBREPRESENTANTE1;
	}

	public String getEBDNIREPRE1() {
		return EBDNIREPRE1;
	}

	public void setEBDNIREPRE1(String eBDNIREPRE1) {
		EBDNIREPRE1 = eBDNIREPRE1;
	}

	public String getEBCARGOREP1() {
		return EBCARGOREP1;
	}

	public void setEBCARGOREP1(String eBCARGOREP1) {
		EBCARGOREP1 = eBCARGOREP1;
	}

	
}
