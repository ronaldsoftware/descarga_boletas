package model;

import java.io.Serializable;

public class SubreporteDiasLaborados implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nombreConcepto;
	private double dias, valor, horas;

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getNombreConcepto() {
		return nombreConcepto;
	}

	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}

	public double getDias() {
		return dias;
	}

	public void setDias(double dias) {
		this.dias = dias;
	}

	public double getHoras() {
		return horas;
	}

	public void setHoras(double horas) {
		this.horas = horas;
	}


}
