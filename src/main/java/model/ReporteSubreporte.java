package model;

import java.io.Serializable;

public class ReporteSubreporte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String descripcion, monto, codtra, codcon, nomvalpar, tipovalpar;

	public String getTipovalpar() {
		return tipovalpar;
	}

	public void setTipovalpar(String tipovalpar) {
		this.tipovalpar = tipovalpar;
	}

	public String getNomvalpar() {
		return nomvalpar;
	}

	public void setNomvalpar(String nomvalpar) {
		this.nomvalpar = nomvalpar;
	}

	public String getCodtra() {
		return codtra;
	}

	public void setCodtra(String codtra) {
		this.codtra = codtra;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getCodcon() {
		return codcon;
	}

	public void setCodcon(String codcon) {
		this.codcon = codcon;
	}

}
