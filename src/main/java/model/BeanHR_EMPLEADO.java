package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BeanHR_EMPLEADO implements Serializable {
	private String EMPCODCIA, EMPCODSUC, EMPCODTRA, EMPNOMBRE, EMPAPATERN,
			EMPAMATERN, EMPFLGEST, EMPEMAIL, EMPNROPER, MES, ANIO,EMPCODPRO, RUTA,EMPNRODOCID,EMPCODLOC, EMPTIPOEMP;
			;

	public String getEMPNRODOCID() {
		return EMPNRODOCID;
	}

	public void setEMPNRODOCID(String eMPNRODOCID) {
		EMPNRODOCID = eMPNRODOCID;
	}

	public String getANIO() {
		return ANIO;
	}

	public void setANIO(String aNIO) {
		ANIO = aNIO;
	}



	public String getEMPTIPOEMP() {
		return EMPTIPOEMP;
	}

	public void setEMPTIPOEMP(String eMPTIPOEMP) {
		EMPTIPOEMP = eMPTIPOEMP;
	}

	public String getRUTA() {
		return RUTA;
	}

	public void setRUTA(String rUTA) {
		RUTA = rUTA;
	}

	public String getEMPCODPRO() {
		return EMPCODPRO;
	}

	public void setEMPCODPRO(String eMPCODPRO) {
		EMPCODPRO = eMPCODPRO;
	}

	public String getMES() {
		return MES;
	}

	public void setMES(String mES) {
		MES = mES;
	}

	public String getEMPCODCIA() {
		return EMPCODCIA;
	}

	public void setEMPCODCIA(String eMPCODCIA) {
		EMPCODCIA = eMPCODCIA;
	}

	public String getEMPCODSUC() {
		return EMPCODSUC;
	}

	public void setEMPCODSUC(String eMPCODSUC) {
		EMPCODSUC = eMPCODSUC;
	}

	public String getEMPCODTRA() {
		return EMPCODTRA;
	}

	public void setEMPCODTRA(String eMPCODTRA) {
		EMPCODTRA = eMPCODTRA;
	}

	public String getEMPNOMBRE() {
		return EMPNOMBRE;
	}

	public void setEMPNOMBRE(String eMPNOMBRE) {
		EMPNOMBRE = eMPNOMBRE;
	}

	public String getEMPAPATERN() {
		return EMPAPATERN;
	}

	public void setEMPAPATERN(String eMPAPATERN) {
		EMPAPATERN = eMPAPATERN;
	}

	public String getEMPAMATERN() {
		return EMPAMATERN;
	}

	public void setEMPAMATERN(String eMPAMATERN) {
		EMPAMATERN = eMPAMATERN;
	}

	public String getEMPFLGEST() {
		return EMPFLGEST;
	}

	public void setEMPFLGEST(String eMPFLGEST) {
		EMPFLGEST = eMPFLGEST;
	}

	public String getEMPEMAIL() {
		return EMPEMAIL;
	}

	public void setEMPEMAIL(String eMPEMAIL) {
		EMPEMAIL = eMPEMAIL;
	}

	public String getEMPNROPER() {
		return EMPNROPER;
	}

	public void setEMPNROPER(String eMPNROPER) {
		EMPNROPER = eMPNROPER;
	}

	public String getEMPCODLOC() {
		return EMPCODLOC;
	}

	public void setEMPCODLOC(String eMPCODLOC) {
		EMPCODLOC = eMPCODLOC;
	}
	
	

}
