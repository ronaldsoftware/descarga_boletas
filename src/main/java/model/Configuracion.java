package model;

public class Configuracion {

	private String compania;
	private String proceso;
	private String periodo;
	private String rutaDescarga;
	private boolean cifrado;
	private boolean boletaDoble;

	public boolean isBoletaDoble() {
		return boletaDoble;
	}

	public void setBoletaDoble(boolean boletaDoble) {
		this.boletaDoble = boletaDoble;
	}

	public boolean isCifrado() {
		return cifrado;
	}

	public void setCifrado(boolean cifrado) {
		this.cifrado = cifrado;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getRutaDescarga() {
		return rutaDescarga;
	}

	public void setRutaDescarga(String rutaDescarga) {
		this.rutaDescarga = rutaDescarga;
	}

}
