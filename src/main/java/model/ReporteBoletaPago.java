package model;

import java.io.Serializable;
import java.util.ArrayList;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@SuppressWarnings({ "rawtypes", "serial" })
public class ReporteBoletaPago implements Serializable {

	private String razon;
	private String remype;
	private String dom;
	private String ruc;
	private String nombre;
	private String nombreE;
	private String apellidoP;
	private String apellidoM;
	private String ccosto;
	private String cargo;
	private String local;
	private String area;
	private String fecing;
	private String dialab;
	private String essalud;
	private String sobretiempo;
	private String normales;
	private String sueldo;
	private String categoria;
	private String dni;
	private String tipodocid;
	private String spp;
	private String tardanzas;
	private String afp;
	private String cusp;
	private String calificacion;
	private String pervac;
	private String salida;
	private String retorno;
	private String codigo;
	private String tremu;
	private String tdsc;
	private String tapor;
	private String neto;
	private String Rfecha;
	private String banco;
	private String nrocuenta;
	private String asig;
	private String fecret;
	private String fecha;
	private String periodo;
	private String codant;
	private String fecfincon;
	private String bancocts;
	private String nroctcts;
	private String empflagdata;
	private String hnormales;
	private String h25;
	private String h35;
	private String h100;
	private String anio_periodo;
	private String mes_periodo;
	private String totalprestamo;
	private String pagadoprestamo;
	private String saldoprestamo;
	private String DMEDL;
	private String telefono;
	private String netoletra;
	private String codtra;
	private String codcia;
	private String codsuc;
	private String pini;
	private String pfin;
	private String pcodpuest;
	private String pcodarea;
	private String pcodloc;
	private String pccosto;
	private String pro;
	private String dias_pag;
	private String hrex35dia;
	private String hrex35noc;
	private String semana;
	private String representante;
	private String cargorep;
	private String situesp;
	private String SUBMATERN;
	private String SUBENFER;
	private String H50;
	private String fecpago;
	private String DFALTA;
	private String DPERM;
	private String DSGH;
	private String DCGH;
	private String DSUSP;
	private String DFERIADO;
	private String DVACSALDO;
	private String LIC_TRAB;
	private String gananciaTotal;
	private String deduccionTotal;
	private String montoEssalud;
	private String montoSenati;
	private String montoEPS;
	private String montoAportAdic1;
	private String sexo;
	private String fecnac;
	private String paisnac;
	private String direccion;
	private String catocup;
	private String departamento;
	private String provincia;
	private String distrito;
	private String empcodafp;
	private String mesNumero;

	private double ncomisiones, ndescremu;

	public String getEmpcodafp() {
		return empcodafp;
	}

	public void setEmpcodafp(String empcodafp) {
		this.empcodafp = empcodafp;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public double getNcomisiones() {
		return ncomisiones;
	}

	public void setNcomisiones(double ncomisiones) {
		this.ncomisiones = ncomisiones;
	}

	public double getNdescremu() {
		return ndescremu;
	}

	public void setNdescremu(double ndescremu) {
		this.ndescremu = ndescremu;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getFecnac() {
		return fecnac;
	}

	public void setFecnac(String fecnac) {
		this.fecnac = fecnac;
	}

	public String getPaisnac() {
		return paisnac;
	}

	public void setPaisnac(String paisnac) {
		this.paisnac = paisnac;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCatocup() {
		return catocup;
	}

	public void setCatocup(String catocup) {
		this.catocup = catocup;
	}

	private double valorHorasReducidas;

	public double getValorHorasReducidas() {
		return valorHorasReducidas;
	}

	public void setValorHorasReducidas(double valorHorasReducidas) {
		this.valorHorasReducidas = valorHorasReducidas;
	}

	public String getNombreCompleto() {
		return apellidoP + " " + apellidoM + " " + nombreE;
	}

	public String getGananciaTotal() {
		return gananciaTotal;
	}

	public void setGananciaTotal(String gananciaTotal) {
		this.gananciaTotal = gananciaTotal;
	}

	public String getDeduccionTotal() {
		return deduccionTotal;
	}

	public void setDeduccionTotal(String deduccionTotal) {
		this.deduccionTotal = deduccionTotal;
	}

	public String getMontoEssalud() {
		return montoEssalud;
	}

	public void setMontoEssalud(String montoEssalud) {
		this.montoEssalud = montoEssalud;
	}

	public String getMontoSenati() {
		return montoSenati;
	}

	public void setMontoSenati(String montoSenati) {
		this.montoSenati = montoSenati;
	}

	public String getMontoEPS() {
		return montoEPS;
	}

	public void setMontoEPS(String montoEPS) {
		this.montoEPS = montoEPS;
	}

	public String getMontoAportAdic1() {
		return montoAportAdic1;
	}

	public void setMontoAportAdic1(String montoAportAdic1) {
		this.montoAportAdic1 = montoAportAdic1;
	}

	/**
	 * @return the semana
	 */
	public String getSemana() {
		return semana;
	}

	/**
	 * @param semana the semana to set
	 */
	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getCodcia() {
		return codcia;
	}

	public void setCodcia(String codcia) {
		this.codcia = codcia;
	}

	public String getCodsuc() {
		return codsuc;
	}

	public void setCodsuc(String codsuc) {
		this.codsuc = codsuc;
	}

	public String getPini() {
		return pini;
	}

	public void setPini(String pini) {
		this.pini = pini;
	}

	public String getPfin() {
		return pfin;
	}

	public void setPfin(String pfin) {
		this.pfin = pfin;
	}

	public String getPcodpuest() {
		return pcodpuest;
	}

	public void setPcodpuest(String pcodpuest) {
		this.pcodpuest = pcodpuest;
	}

	public String getPcodarea() {
		return pcodarea;
	}

	public void setPcodarea(String pcodarea) {
		this.pcodarea = pcodarea;
	}

	public String getPcodloc() {
		return pcodloc;
	}

	public void setPcodloc(String pcodloc) {
		this.pcodloc = pcodloc;
	}

	public String getPccosto() {
		return pccosto;
	}

	public void setPccosto(String pccosto) {
		this.pccosto = pccosto;
	}

	public String getPro() {
		return pro;
	}

	public void setPro(String pro) {
		this.pro = pro;
	}

	private ArrayList listaRemuneracion = null;
	private ArrayList listaDeduccion = null;
	private ArrayList listaAportacion = null;
	private ArrayList listaVacaciones = null;
	private ArrayList listaDiasLaborados = null;
	private ArrayList listaMontosAdicionales = null;
	private ArrayList listaAsistencias = null;

	public ArrayList getListaAsistencias() {
		return listaAsistencias;
	}

	public void setListaAsistencias(ArrayList listaAsistencias) {
		this.listaAsistencias = listaAsistencias;
	}

	public ArrayList getListaVacaciones() {
		return listaVacaciones;
	}

	public void setListaVacaciones(ArrayList listaVacaciones) {
		this.listaVacaciones = listaVacaciones;
	}

	public ArrayList getListaDiasLaborados() {
		return listaDiasLaborados;
	}

	public void setListaDiasLaborados(ArrayList listaDiasLaborados) {
		this.listaDiasLaborados = listaDiasLaborados;
	}

	public ArrayList getListaMontosAdicionales() {
		return listaMontosAdicionales;
	}

	public void setListaMontosAdicionales(ArrayList listaMontosAdicionales) {
		this.listaMontosAdicionales = listaMontosAdicionales;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDMEDL() {
		return DMEDL;
	}

	public void setDMEDL(String dMEDL) {
		DMEDL = dMEDL;
	}

	public String getTotalprestamo() {
		return totalprestamo;
	}

	public void setTotalprestamo(String totalprestamo) {
		this.totalprestamo = totalprestamo;
	}

	public String getPagadoprestamo() {
		return pagadoprestamo;
	}

	public void setPagadoprestamo(String pagadoprestamo) {
		this.pagadoprestamo = pagadoprestamo;
	}

	public String getSaldoprestamo() {
		return saldoprestamo;
	}

	public void setSaldoprestamo(String saldoprestamo) {
		this.saldoprestamo = saldoprestamo;
	}

	public String getHnormales() {
		return hnormales;
	}

	public void setHnormales(String hnormales) {
		this.hnormales = hnormales;
	}

	public String getH25() {
		return h25;
	}

	public void setH25(String h25) {
		this.h25 = h25;
	}

	public String getH35() {
		return h35;
	}

	public void setH35(String h35) {
		this.h35 = h35;
	}

	public String getH100() {
		return h100;
	}

	public void setH100(String h100) {
		this.h100 = h100;
	}

	public String getAnio_periodo() {
		return anio_periodo;
	}

	public void setAnio_periodo(String anio_periodo) {
		this.anio_periodo = anio_periodo;
	}

	public String getMes_periodo() {
		return mes_periodo;
	}

	public void setMes_periodo(String mes_periodo) {
		this.mes_periodo = mes_periodo;
	}

	public String getPervac() {
		return pervac;
	}

	public void setPervac(String pervac) {
		this.pervac = pervac;
	}

	public String getBancocts() {
		return bancocts;
	}

	public void setBancocts(String bancocts) {
		this.bancocts = bancocts;
	}

	public String getCodant() {
		return codant;
	}

	public void setCodant(String codant) {
		this.codant = codant;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNrocuenta() {
		return nrocuenta;
	}

	public void setNrocuenta(String nrocuenta) {
		this.nrocuenta = nrocuenta;
	}

	public String getCodtra() {
		return codtra;
	}

	public void setCodtra(String codtra) {
		this.codtra = codtra;
	}

	public ArrayList getListaDeduccion() {
		return listaDeduccion;
	}

	public void setListaDeduccion(ArrayList listaDeduccion) {
		this.listaDeduccion = listaDeduccion;
	}

	public ArrayList getListaAportacion() {
		return listaAportacion;
	}

	public void setListaAportacion(ArrayList listaAportacion) {
		this.listaAportacion = listaAportacion;
	}

	public ArrayList getListaRemuneracion() {
		return listaRemuneracion;
	}

	public void setListaRemuneracion(ArrayList listaRemuneracion) {
		this.listaRemuneracion = listaRemuneracion;
	}

	public String getRazon() {
		return razon;
	}

	public void setRazon(String razon) {
		this.razon = razon;
	}

	public String getRemype() {
		return remype;
	}

	public void setRemype(String remype) {
		this.remype = remype;
	}

	public String getDom() {
		return dom;
	}

	public void setDom(String dom) {
		this.dom = dom;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFecing() {
		return fecing;
	}

	public void setFecing(String fecing) {
		this.fecing = fecing;
	}

	public String getDialab() {
		return dialab;
	}

	public void setDialab(String dialab) {
		this.dialab = dialab;
	}

	public String getEssalud() {
		return essalud;
	}

	public void setEssalud(String essalud) {
		this.essalud = essalud;
	}

	public String getSobretiempo() {
		return sobretiempo;
	}

	public void setSobretiempo(String sobretiempo) {
		this.sobretiempo = sobretiempo;
	}

	public String getNormales() {
		return normales;
	}

	public void setNormales(String normales) {
		this.normales = normales;
	}

	public String getSueldo() {
		return sueldo;
	}

	public void setSueldo(String sueldo) {
		this.sueldo = sueldo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTipodocid() {
		return tipodocid;
	}

	public void setTipodocid(String tipodocid) {
		this.tipodocid = tipodocid;
	}

	public String getSpp() {
		return spp;
	}

	public void setSpp(String spp) {
		this.spp = spp;
	}

	public String getTardanzas() {
		return tardanzas;
	}

	public void setTardanzas(String tardanzas) {
		this.tardanzas = tardanzas;
	}

	public String getAfp() {
		return afp;
	}

	public void setAfp(String afp) {
		this.afp = afp;
	}

	public String getCusp() {
		return cusp;
	}

	public void setCusp(String cusp) {
		this.cusp = cusp;
	}

	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public String getRetorno() {
		return retorno;
	}

	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTremu() {
		return tremu;
	}

	public void setTremu(String tremu) {
		this.tremu = tremu;
	}

	public String getTdsc() {
		return tdsc;
	}

	public void setTdsc(String tdsc) {
		this.tdsc = tdsc;
	}

	public String getTapor() {
		return tapor;
	}

	public void setTapor(String tapor) {
		this.tapor = tapor;
	}

	public String getNeto() {
		return neto;
	}

	public void setNeto(String neto) {
		this.neto = neto;
	}

	public String getRfecha() {
		return Rfecha;
	}

	public void setRfecha(String rfecha) {
		Rfecha = rfecha;
	}

	public String getNombreE() {
		return nombreE;
	}

	public void setNombreE(String nombreE) {
		this.nombreE = nombreE;
	}

	public String getApellidoP() {
		return apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}

	public String getApellidoM() {
		return apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}

	public String getAsig() {
		return asig;
	}

	public void setAsig(String asig) {
		this.asig = asig;
	}

	public String getFecret() {
		return fecret;
	}

	public void setFecret(String fecret) {
		this.fecret = fecret;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFecfincon() {
		return fecfincon;
	}

	public void setFecfincon(String fecfincon) {
		this.fecfincon = fecfincon;
	}

	public String getNroctcts() {
		return nroctcts;
	}

	public void setNroctcts(String nroctcts) {
		this.nroctcts = nroctcts;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEmpflagdata() {
		return empflagdata;
	}

	public void setEmpflagdata(String empflagdata) {
		this.empflagdata = empflagdata;
	}

	public String getCcosto() {
		return ccosto;
	}

	public void setCcosto(String ccosto) {
		this.ccosto = ccosto;
	}

	public String getDias_pag() {
		return dias_pag;
	}

	public void setDias_pag(String dias_pag) {
		this.dias_pag = dias_pag;
	}

	public String getHrex35dia() {
		return hrex35dia;
	}

	public void setHrex35dia(String hrex35dia) {
		this.hrex35dia = hrex35dia;
	}

	public String getHrex35noc() {
		return hrex35noc;
	}

	public void setHrex35noc(String hrex35noc) {
		this.hrex35noc = hrex35noc;
	}

	public String getNetoletra() {
		return netoletra;
	}

	public void setNetoletra(String netoletra) {
		this.netoletra = netoletra;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getCargorep() {
		return cargorep;
	}

	public void setCargorep(String cargorep) {
		this.cargorep = cargorep;
	}

	public String getSituesp() {
		return situesp;
	}

	public void setSituesp(String situesp) {
		this.situesp = situesp;
	}

	public String getSUBMATERN() {
		return SUBMATERN;
	}

	public void setSUBMATERN(String sUBMATERN) {
		SUBMATERN = sUBMATERN;
	}

	public String getSUBENFER() {
		return SUBENFER;
	}

	public void setSUBENFER(String sUBENFER) {
		SUBENFER = sUBENFER;
	}

	public String getH50() {
		return H50;
	}

	public void setH50(String h50) {
		H50 = h50;
	}

	public String getFecpago() {
		return fecpago;
	}

	public void setFecpago(String fecpago) {
		this.fecpago = fecpago;
	}

	public String getDFALTA() {
		return DFALTA;
	}

	public void setDFALTA(String dFALTA) {
		DFALTA = dFALTA;
	}

	public String getDPERM() {
		return DPERM;
	}

	public void setDPERM(String dPERM) {
		DPERM = dPERM;
	}

	public String getDSGH() {
		return DSGH;
	}

	public void setDSGH(String dSGH) {
		DSGH = dSGH;
	}

	public String getDCGH() {
		return DCGH;
	}

	public void setDCGH(String dCGH) {
		DCGH = dCGH;
	}

	public String getDSUSP() {
		return DSUSP;
	}

	public void setDSUSP(String dSUSP) {
		DSUSP = dSUSP;
	}

	public String getDFERIADO() {
		return DFERIADO;
	}

	public void setDFERIADO(String dFERIADO) {
		DFERIADO = dFERIADO;
	}

	public String getDVACSALDO() {
		return DVACSALDO;
	}

	public void setDVACSALDO(String dVACSALDO) {
		DVACSALDO = dVACSALDO;
	}

	public String getLIC_TRAB() {
		return LIC_TRAB;
	}

	public void setLIC_TRAB(String lIC_TRAB) {
		LIC_TRAB = lIC_TRAB;
	}

	public JRDataSource getvacaciones() {
		return new JRBeanCollectionDataSource(listaVacaciones);
	}

	public JRDataSource getdiaslaborados() {
		return new JRBeanCollectionDataSource(listaDiasLaborados);
	}

	public JRDataSource getremuneracion() {
		return new JRBeanCollectionDataSource(listaRemuneracion);
	}

	public JRDataSource getasistencias() {
		return new JRBeanCollectionDataSource(listaAsistencias);
	}

	public JRDataSource getdeduccion() {
		return new JRBeanCollectionDataSource(listaDeduccion);
	}

	public JRDataSource getaportacion() {
		return new JRBeanCollectionDataSource(listaAportacion);
	}

	public JRDataSource getmontosadicionales() {
		return new JRBeanCollectionDataSource(listaMontosAdicionales);
	}

	public JRDataSource getremuneracion2() {
		return new JRBeanCollectionDataSource(listaRemuneracion);
	}

	public JRDataSource getdeduccion2() {
		return new JRBeanCollectionDataSource(listaDeduccion);
	}

	public JRDataSource getaportacion2() {
		return new JRBeanCollectionDataSource(listaAportacion);
	}

	public JRDataSource getvacaciones2() {
		return new JRBeanCollectionDataSource(listaVacaciones);
	}

	public JRDataSource getdiaslaborados2() {
		return new JRBeanCollectionDataSource(listaDiasLaborados);
	}

	public JRDataSource getmontosadicionales2() {
		return new JRBeanCollectionDataSource(listaMontosAdicionales);
	}

	public String getMesNumero() {
		return mesNumero;
	}

	public void setMesNumero(String mesNumero) {
		this.mesNumero = mesNumero;
	}
}
