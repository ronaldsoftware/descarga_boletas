package constants;

public class ConstantesCompania {

	// INTRALOT
	public static final String COMPANIA_INTRALOT = "026";
	public static final String NOMBRE_INTRALOT = "INTRALOT";
	public static final String PROPERTIES_INTRALOT = "properties/intralot.properties";

	// COMSATEL
	public static final String COMPANIA_COMSATEL = "031";
	public static final String NOMBRE_COMSATEL = "COMSATEL";
	public static final String PROPERTIES_COMSATEL = "properties/comsatel.properties";

	// PURATOS
	public static final String COMPANIA_PURATOS = "066";
	public static final String NOMBRE_PURATOS = "PURATOS";
	public static final String PROPERTIES_PURATOS = "properties/puratos.properties";

	// UNICON
	public static final String PROPERTIES_UNICON = "properties/unicon.properties";
	public static final String COMPANIA_UNICON = "061";
	public static final String NOMBRE_UNICON = "UNICON";

	public static final String COMPANIA_CONCREMAX = "062";
	public static final String NOMBRE_CONCREMAX = "CONCREMAX";

	public static final String COMPANIA_ENTREPISOS = "063";
	public static final String NOMBRE_ENTREPISOS = "ENTRE PISOS";

	public static final String COMPANIA_AGRECOM = "064";
	public static final String NOMBRE_AGRECOM = "AGRECOM";

	public static final String COMPANIA_BASF = "065";
	public static final String NOMBRE_BASF = "BASF";
}
