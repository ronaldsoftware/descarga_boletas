package constants;

public class Constantes {

	public static final String ESTADO_PENDIENTE = "0";
	public static final String ESTADO_PROCESADO = "1";
	public static final String ESTADO_ENVIADO = "2";
	public static final String CUALQUIERA = "%";

	public static final String EXTENSION_PDF = ".pdf";
	public static final String SLASH = "/";
	public static final String GUION_BAJO = "_";
	public static final String COMA = ",";

	public static final String SUCURSAL_DEFAULT = "001";
}
